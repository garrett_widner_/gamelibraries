﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class LocationChecker : MonoBehaviour
{
    public BoxCollider2D collider;
    public LayerMask obstructionLayers;

    public bool debugOn = false;

    public void Update()
    {
        if (debugOn)
        {
            TestWithMouseClick();
        }
    }

    public bool CheckIfLocationClear(Vector2 location)
    {
        Vector2 extents = new Vector2(collider.bounds.extents.x, collider.bounds.extents.y);
        Vector2 upperLeft = new Vector2(location.x - extents.x, location.y + extents.y);
        Vector2 lowerRight = new Vector2(location.x + extents.x, location.y - extents.y);

        Collider2D foundCollider = null;
        foundCollider = Physics2D.OverlapArea(upperLeft, lowerRight, obstructionLayers);

        if (debugOn)
        {
            SuperDebugger.DrawPlusAtPoint(upperLeft, Color.blue, .5f, 5f);
            SuperDebugger.DrawPlusAtPoint(lowerRight, Color.red, .5f, 5f);
        }

        if (foundCollider != null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    private void TestWithMouseClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 screenPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 locotion = new Vector2(screenPosition.x, screenPosition.y);
            CheckIfLocationClear(locotion);
        }
    }
}
