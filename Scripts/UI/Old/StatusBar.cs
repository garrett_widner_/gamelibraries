﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StatusBar : StatusTracker
{
    [Space(10)]
    [Tooltip("Note: healthFillRect must be set to full maximum width at scene start")]
    public RectTransform fill;
    public RectTransform barContainer;
    private float maxWidthWhenFull;
    private float containerStartWidth;
    private float barWidthToMaxStatLevelRatio;

    [Header("Damage Flash")]
    public bool useDamageFlash = false;
    [Tooltip("Should be a UI image that covers the entire screen")]
    public Image damageFlashImage;
    public float damageFlashSpeed = 5f;
    public Color damageFlashColor = new Color(1f, 0f, 0f, 0.1f);
    private float minDamageForFlash = 3.5f;

    [Header("Change Rates")]
    public float damageRateOverTime = 30f;
    public float healingRateOverTime = 10f;
    public float maxHealthChangeRateOverTime = 20f;

    //[Space(10)]
    //public float testValue = 40f;


    protected override void Start()
    {
        base.Start();
        timedHealingStopsWhenReachingExtremes = true;
        containerStartWidth = barContainer.rect.width;
        barWidthToMaxStatLevelRatio = containerStartWidth / MaxStatLevel;
    }

    protected override void Update()
    {
        base.Update();

        //TestTimedHealing();

        UpdateSliderImage();

        UpdateDamageFlash();
    }

    /*
    private void TestTimedHealing()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            IncreaseMaxOverTime(testValue);
            print("Increasing max stat level by " + testValue + " over time.");
        }
        else if (Input.GetKeyDown(KeyCode.Z))
        {
            DecreaseMaxOverTime(testValue);
            print("Decreasing max stat level by " + testValue + " over time.");
        }
        else if (Input.GetKeyDown(KeyCode.M))
        {
            HealOverTime(testValue);
            print("Healing " + testValue + " over time");
        }
        else if (Input.GetKeyDown(KeyCode.N))
        {
            TakeDamageOverTime(testValue);
            print("Damaging " + testValue + " over time");
        }
        else if (Input.GetKeyDown(KeyCode.C))
        {
            SetCurrentStatLevelImmediate(testValue);
            print("Current stat level: " + CurrentStatLevel);
        }
    }
    */

    private void UpdateSliderImage()
    {
        maxWidthWhenFull = barContainer.rect.width;

        //Current Stat Level
        float currentStatRatio = CurrentStatLevel / MaxStatLevel;
        float statFillWidth = currentStatRatio * maxWidthWhenFull;
        fill.sizeDelta = new Vector2(statFillWidth, fill.sizeDelta.y);

        //Overall (Max) Stat Level
        float containerWidth = barWidthToMaxStatLevelRatio * MaxStatLevel;
        barContainer.sizeDelta = new Vector2(containerWidth, barContainer.sizeDelta.y);
    }

    private void UpdateDamageFlash()
    {
        if(useDamageFlash && damageFlashImage != null)
        {
            damageFlashImage.color = Color.Lerp(damageFlashImage.color, Color.clear, damageFlashSpeed * Time.deltaTime);
        }
    }

    #region Taking Damage
    public void TakeDamageImmediate(float damage)
    {
        DecreaseStatLevelImmediate(damage);
        CheckForDamageFlash(damage);
    }

    private void CheckForDamageFlash(float damage)
    {
        if (useDamageFlash && damage > minDamageForFlash && damageFlashImage != null)
        {
            damageFlashImage.color = damageFlashColor;
        }
    }

    public void TakeDamageOverTime(float damage)
    {
        float seconds = damage / damageRateOverTime;
        DecreaseStatLevelOverTime(damage, seconds);
        CheckForDamageFlash(damage);
    }
    #endregion

    #region Healing
    public void HealImmediate(float amount)
    {
        IncreaseStatLevelImmediate(amount);
    }

    public void HealOverTime(float amount)
    {
        float seconds = amount / healingRateOverTime;
        IncreaseStatLevelOverTime(amount, seconds);
    }
    #endregion

    #region Max Health Changes
    public void IncreaseMaxImmediate(float amount)
    {
        IncreaseMaxStatLevelImmediate(amount, true);
    }

    public void IncreaseMaxOverTime(float amount)
    {
        float seconds = amount / maxHealthChangeRateOverTime;
        IncreaseMaxStatLevelOverTime(amount, seconds, true);
    }

    public void DecreaseMaxImmediate(float amount)
    {
        DecreaseMaxStatLevelImmediate(amount);
    }

    public void DecreaseMaxOverTime(float amount)
    {
        float seconds = amount / maxHealthChangeRateOverTime;
        DecreaseMaxStatLevelOverTime(amount, seconds);
    }

    #endregion



}
