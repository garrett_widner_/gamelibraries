﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AnimationSequence : Sequence
{
    public Animator animator;
    public string triggerName;
}
