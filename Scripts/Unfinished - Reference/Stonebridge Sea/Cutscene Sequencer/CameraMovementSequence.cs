﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CameraMovementSequence : Sequence
{
    public Transform moveLocation;
    public CameraMover cameraMover;
    public float speed;
    public bool locksToLocationWhenFinished = false;

}
