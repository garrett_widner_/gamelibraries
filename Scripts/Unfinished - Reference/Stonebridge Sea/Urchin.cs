﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Urchin : MonoBehaviour
{
    public HopMover hopMover;
    public HopHandler hopHandler;
    public Collider2D urchinCollider;

    public int maxHopsUntilStop = 7;
    public int minHopsUntilStop = 4;

    public float waitAfterHop = 1f;
    public float meatExposureTime = 3f;

    private bool isAlive = true;
    private bool isVulnerable = false;
    public bool IsVulnerable
    {
        get
        {
            return isVulnerable;
        }
    }

    public delegate void UrchinAction();
    public event UrchinAction OnBecameVulnerable;
    public event UrchinAction OnBecameInvulnerable;

    private void Start()
    {
        isAlive = true;
        StartCoroutine("HopSequence");
    }

    private void Update()
    {
        if(hopMover.IsHopping)
        {
            hopMover.Move();
        }
    }

    private IEnumerator HopSequence()
    {
        while (isAlive)
        {
            int hopsUntilStop = Random.Range(minHopsUntilStop, maxHopsUntilStop + 1);
            //print(hopsUntilStop);
            for (int i = 0; i < hopsUntilStop; i++)
            {
                hopHandler.HopInRandomDirection();
                yield return new WaitForSeconds(0.1f);
                while(hopMover.IsHopping)
                {
                    yield return new WaitForEndOfFrame();
                }
                if(!hopMover.IsHopping)
                {
                    yield return new WaitForSeconds(waitAfterHop);

                }
            }
            isVulnerable = true;
            if(OnBecameVulnerable != null)
            {
                OnBecameVulnerable();
            }
            urchinCollider.enabled = false;
            yield return new WaitForSeconds(meatExposureTime);
            isVulnerable = false;
            if(OnBecameInvulnerable != null)
            {
                OnBecameInvulnerable();
            }
            urchinCollider.enabled = true;

            yield return null;
        }
    }

    public void EndHopSequence()
    {
        StopAllCoroutines();
    }



}
