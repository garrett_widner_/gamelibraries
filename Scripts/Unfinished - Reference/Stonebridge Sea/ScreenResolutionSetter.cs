﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenResolutionSetter : MonoBehaviour
{
	void Update ()
    {
        if(Mathf.RoundToInt(Time.time) % 7 == 0)
        {
            //Debug.LogWarning("Warning: Using Input key presses to change resoluton Display. Remove in final version.");
        }
        
        /*
		if(Input.GetKeyDown(KeyCode.H))
        {
            ResolutionManager resolutionManager = ResolutionManager.Instance;
            //print("Fullscreen should have been toggled on or off");
            if (ResolutionManager.Instance == null) return;
            //print("And resolutionManager was not null");
            resolutionManager.ToggleFullscreen();
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            DisplayResolutionText();
        }
        */

        if (Input.GetKeyDown(KeyCode.G))
        {
            IncrementResolution();
        }

    }

    private void IncrementResolution()
    {
        ResolutionManager resolutionManager = ResolutionManager.Instance;
        if (ResolutionManager.Instance == null) return;

        int currentIndex = resolutionManager.CurrentResolutionIndex;

        if (Screen.fullScreen)
        {
            resolutionManager.SetResolution(0, false);
        }
        else
        {
            int i = resolutionManager.CurrentResolutionIndex;
            if(i == 0)
            {
                resolutionManager.SetResolution(1, false);
            }
            else if(i == 1)
            {
                resolutionManager.ToggleFullscreen();
            }
            else
            {
                Debug.LogWarning("Screen resolution index took on unexpected value");
            }
        }

        /*
        int i = resolutionManager.CurrentResolutionIndex + 1;
        if (i > resolutionManager.MaxResolutionIndex) i = 0;

        Vector2 oldResolution = resolutionManager.GetResolutionByIndex(resolutionManager.CurrentResolutionIndex);
        Vector2 newResolution = resolutionManager.GetResolutionByIndex(i);

        print("Resolution being set from " + oldResolution.ToString() + " to " + newResolution.ToString());

        resolutionManager.SetResolution(i, Screen.fullScreen);
        */
    }

    private void DisplayResolutionText()
    {
        ResolutionManager resolutionManager = ResolutionManager.Instance;
        if (ResolutionManager.Instance == null) return;

        string allResolutions = "";

        print(Screen.fullScreen ? "Fullscreen Resolutions: " : "Windowed Resolutions: ");

        foreach (Vector2 r in Screen.fullScreen ? resolutionManager.FullscreenResolutions : resolutionManager.WindowedResolutions)
        {
            string label = r.x + "x" + r.y;
            if (r.x == Screen.width && r.y == Screen.height) label += "*";
            if (r.x == resolutionManager.DisplayResolution.width && r.y == resolutionManager.DisplayResolution.height) label += " (native)";

            allResolutions += label + " - ";
        }

        print(allResolutions);
    }
}
