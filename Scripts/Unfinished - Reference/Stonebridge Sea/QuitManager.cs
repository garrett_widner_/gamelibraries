﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitManager : MonoBehaviour
{
    private PlayerActions playerActions;

    public float buttonHoldTimeUntilQuit = 3f;

    private void Start()
    {
        playerActions = PlayerActions.CreateWithDefaultBindings();
    }

    private void Update()
    {
        if(playerActions.Quit.WasPressed)
        {
            QuitButtonPressed();
        }
        else if(playerActions.Quit.WasReleased)
        {
            QuitButtonReleased();
        }
    }

    private void QuitButtonPressed()
    {
        Invoke("QuitTimerEnded", buttonHoldTimeUntilQuit);
    }

    private void QuitButtonReleased()
    {
        CancelInvoke("QuitTimerEnded");
    }

    private void QuitTimerEnded()
    {
        Application.Quit();
    }
}
