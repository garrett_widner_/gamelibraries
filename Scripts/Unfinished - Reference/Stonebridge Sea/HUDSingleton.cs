﻿using UnityEngine;

public class HUDSingleton : MonoBehaviour
{
    public static HUDSingleton instance = null;

    protected virtual void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            print("Multiple Singletons Detected, " + gameObject.name + " destroyed.");
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

}
