﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseManager : MonoBehaviour
{
    public Image pauseImage;
    public GroundMover playerMover;
    public PlayerHopHandler playerHopper;
    public CutsceneSequencer cutsceneSequencer;
    public PlayerPickupCollector playerPickupCollector;
    public PlayerHopHandler playerHopHandler;

    public bool IsPaused
    {
        get
        {
            return isPaused;
        }
    }

    private bool pausingIsLocked = true;
    private bool isPaused = false;
    private bool hasBeenPaused = false;

    private bool freeMovementAtPauseStart;
    private bool freeTurningAtPauseStart;

    private PlayerActions playerActions;

    public delegate void PauseAction();
    public event PauseAction OnPaused;
    public event PauseAction OnUnpause;
    public event PauseAction OnFirstPauseEnd;

    private void OnEnable()
    {
        cutsceneSequencer.OnCutsceneStart += DisablePausing;
        cutsceneSequencer.OnCutsceneEnd += EnablePausing;
        playerHopHandler.OnHopChainEnded += MovementEnabledDuringPause;
    }

    private void OnDisable()
    {
        cutsceneSequencer.OnCutsceneStart -= DisablePausing;
        cutsceneSequencer.OnCutsceneEnd -= EnablePausing;
        playerHopHandler.OnHopChainEnded -= MovementEnabledDuringPause;
    }

    private void Start()
    {
        playerActions = PlayerActions.CreateWithDefaultBindings();
    }

    private void Update()
    {
        if(playerActions.Pause.WasPressed && !pausingIsLocked)
        {
            if(isPaused)
            {
                Unpause();
            }
            else
            {
                Pause();
            }
        }
    }

    private void Pause()
    {
        isPaused = true;
        pauseImage.gameObject.SetActive(true);
        freeMovementAtPauseStart = playerMover.CanMove;
        freeTurningAtPauseStart = playerMover.CanTurn;
        playerMover.DisallowMovement();
        playerMover.DisallowTurning();
        playerHopper.DisallowHopping();
        playerPickupCollector.LockPickup();
        if(OnPaused != null)
        {
            OnPaused();
        }


    }

    private void Unpause()
    {
        isPaused = false;
        pauseImage.gameObject.SetActive(false);
        if(freeMovementAtPauseStart)
        {
            playerMover.AllowMovement();
        }
        if(freeTurningAtPauseStart)
        {
            playerMover.AllowTurning();
        }
        playerHopper.AllowHopping();

        if(OnUnpause != null)
        {
            OnUnpause();
        }
        playerPickupCollector.UnlockPickup();

        if (!hasBeenPaused)
        {
            hasBeenPaused = true;
            if (OnFirstPauseEnd != null)
            {
                OnFirstPauseEnd();
            }
        }
    }

    public void EnablePausing()
    {
        pausingIsLocked = false;
    }

    public void DisablePausing()
    {
        pausingIsLocked = true;
    }

    private void MovementEnabledDuringPause()
    {
        freeMovementAtPauseStart = true;
    }
}
