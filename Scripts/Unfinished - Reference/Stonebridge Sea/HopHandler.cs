﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HopHandler : MonoBehaviour
{
    public HopMover hopMover;

    public bool hasStartHoppable = true;
    public Hoppable startHoppable;

    private Hoppable currentHoppable;
    private Hoppable nextHoppable;
    private ShuffleBag<int> shuffleBag;

    private bool isCurrentlyHopping = false;

    //private Collider2D previousFoundCollider;

    public delegate void HopHandlerAction();
    public event HopHandlerAction OnHopChainStarted;
    public event HopHandlerAction OnHopChainEnded;

    private void OnEnable()
    {
        hopMover.OnHopEnded += HopEnded;
    }

    private void OnDisable()
    {
        hopMover.OnHopEnded -= HopEnded;
    }

    private void Awake()
    {
        if(hasStartHoppable)
        {
            transform.position = startHoppable.HopPoint;
            currentHoppable = startHoppable;
        }
    }

    public void HopInRandomDirection(bool isHighHop = false)
    {
        Vector2 randomHopDirection = FindRandomHopDirection();
        if(randomHopDirection == Vector2.zero)
        {
            Debug.LogWarning("Warning: could not find a place to hop");
            return;
        }

        if(!isHighHop)
        {
            MakeNextHopInChain(randomHopDirection, hopMover.baseHopHeight);
        }
        else
        {
            MakeNextHopInChain(randomHopDirection, 3f);
        }
    }

    private Vector2 FindRandomHopDirection()
    {
        bool hasFoundRandomHopDirection = false;
        Vector2 foundDirection = Vector2.zero;

        List<int> untriedDirections = new List<int>() { 1, 2, 3, 4 };
        shuffleBag = new ShuffleBag<int>(untriedDirections, false);

        while (!shuffleBag.BagIsEmpty && !hasFoundRandomHopDirection)
        {
            int randomDirectionalInt = shuffleBag.GetNextItemInBag();
            
            foundDirection = FindCardinalDirectionFromInt(randomDirectionalInt);
            if(currentHoppable.IsThereAHoppableInDirection(foundDirection))
            {
                if(currentHoppable.GetHoppableInDirection(foundDirection).isPassable)
                {
                    hasFoundRandomHopDirection = true;
                }
            }
            else
            {
                foundDirection = Vector2.zero;
            }
        }

        return foundDirection;
    }

    private Vector2 FindCardinalDirectionFromInt(int direction)
    {
        if(direction != 1 && direction != 2 && direction != 3 && direction != 4)
        {
            Debug.LogWarning("Warning: must enter an int 1-4");
        }
        else
        {
            switch (direction)
            {
                case 1:
                    return Vector2.right;
                case 2:
                    return Vector2.down;
                case 3:
                    return Vector2.left;
                case 4:
                    return Vector2.up;
            }
        }

        return Vector2.zero;
    }

    private void MakeNextHopInChain(Vector2 moveDirection, float height = 1f)
    {
        Vector2 desiredMoveDirection = moveDirection.ClosestCardinalDirectionNormalized();
        if (currentHoppable.IsThereAHoppableInDirection(desiredMoveDirection))
        {
            Hoppable destination = currentHoppable.GetHoppableInDirection(desiredMoveDirection);
            //SuperDebugger.DrawPlusAtPoint(destination.HopPoint, Color.magenta, 0.9f, 5f);
            HopStarted(destination);
        }
    }

    private void HopStarted(Hoppable destination, float height = 1f)
    {
        isCurrentlyHopping = true;

        currentHoppable = null;
        nextHoppable = destination;
        hopMover.StartDurationBasedHop(destination.HopPoint, 1f);
    }

    private void HopEnded()
    {
        isCurrentlyHopping = false;

        currentHoppable = nextHoppable;
        nextHoppable = null;
    }


}

