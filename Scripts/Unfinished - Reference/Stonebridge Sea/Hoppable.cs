﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hoppable : MonoBehaviour
{
    public Hoppable up;
    public Hoppable right;
    public Hoppable down;
    public Hoppable left;
    public bool isPassable = true;
    public bool isStartingPointForKnockback = false;

    [SerializeField]private Transform centerPoint;

    public Vector2 HopPoint
    {
        get
        {
            if(centerPoint != null)
            {
                return centerPoint.position;
            }
            return transform.position;
        }
    }

    public bool isStartingPad = false;

    public Hoppable GetHoppableInDirection(Vector2 direction)
    {
        direction = direction.ClosestCardinalDirection();

        if(direction == Vector2.up)
        {
            return up;
        }
        else if(direction == Vector2.right)
        {
            return right;
        }
        else if(direction == Vector2.down)
        {
            return down;
        }
        else if(direction == Vector2.left)
        {
            return left;
        }

        return null;
    }

    public bool IsThereAHoppableInDirection(Vector2 direction)
    {
        Hoppable hoppable = GetHoppableInDirection(direction);
        if(hoppable != null)
        {
            return true;
        }
        return false;
    }
	
}
