﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[RequireComponent(typeof(ClimbController2D))]
public class Climber : Mover 
{
    private bool canClimb;

    private bool isClimbing = false;
    public bool IsClimbing
    {
        get { return isClimbing; }
    }

    public LayerMask whatIsClimbable;

    private float climbVerticalInputThreshold = 0.85f;

    private WallInfo potentialWallInfo;
    [HideInInspector]public WallInfo currentWallInfo;
    private WallInfo previousWallInfo;
    private float previousWallMemoryLength = 0.3f;

    private bool isInTransitionArea = false;

    private bool isSnappedToClimbable;
    private float snapDistanceLeeway = 0.05f;
    public BoxCollider2D climbCheckArea;
    public BoxCollider2D ledgeCheckArea;

    private float meshClimbSpeed = 3;
    private float ladderClimbSpeed = 4;
    private float ledgeClimbSpeed = 3;
    private float wallClimbSpeed = 3f;
    private float ceilingClimbSpeed = 2f;

    private ClimbController2D climbController;
    private Runner runner;

    private Vector2 currentInput;
    private Vector2 previousInput;
    private bool isHoldingDirectionSinceClimbTransition = false;
    private bool justTransitionedBetweenWallTypes = false;

    protected void Start()
    {
        base.Start();

        climbController = GetComponent<ClimbController2D>();
        runner = GetComponent<Runner>();
    }

    public override void RunMovement()
    {
        SnapIfNecessary();

        currentInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        CheckIfHoldingDirectionSinceClimbTransition(currentInput);

        

        LogInput(currentInput);

        justTransitionedBetweenWallTypes = false;

        if (currentWallInfo.climbCollider != null && potentialWallInfo.climbCollider != null)
        {
            isInTransitionArea = true;
            CheckForClimbTypeChange(currentInput);
        }
        else
        {
            isInTransitionArea = false;
        }

        ModifyInput(ref currentInput);

        switch (currentWallInfo.climbType)
        {
            case ClimbType.BackWall:
                MoveAcrossBackWall(currentInput);
                HandleBackWallJumping(currentInput);
                controller.Move(velocity);
                break;
            case ClimbType.Horizontal:
                MoveAcrossLedge(currentInput);
                HandleBackWallJumping(currentInput);
                controller.Move(velocity);
                break;
            case ClimbType.Vertical:
                MoveAcrossVertical(currentInput);
                HandleBackWallJumping(currentInput);
                controller.Move(velocity);
                break;
            case ClimbType.SideWall:
                MoveAcrossSideWall(currentInput);
                HandleSideWallJumping(currentInput);
                climbController.Move(velocity, currentInput, currentWallInfo.wallFacingDirection);
                break;
            case ClimbType.Ceiling:
                MoveAcrossCeiling(currentInput);
                HandleSideWallJumping(currentInput);
                climbController.Move(velocity, currentInput, currentWallInfo.wallFacingDirection);
                break;
        }
        
        velocity = Vector3.zero;
    }

    private void MoveAcrossBackWall(Vector2 input)
    {
        velocity.x += meshClimbSpeed * input.x * Time.deltaTime;
        velocity.y += meshClimbSpeed * input.y * Time.deltaTime;
    }

    private void MoveAcrossLedge(Vector2 input)
    {
        velocity.x += ledgeClimbSpeed * input.x * Time.deltaTime;
    }

    private void MoveAcrossVertical(Vector2 input)
    {
        velocity.y += ladderClimbSpeed * input.y * Time.deltaTime;
    }

    private void MoveAcrossSideWall(Vector2 input)
    {
        velocity.y += wallClimbSpeed * input.y * Time.deltaTime;
    }

    private void MoveAcrossCeiling(Vector2 input)
    {
        velocity.x += ceilingClimbSpeed * input.x * Time.deltaTime;
    }

    private void HandleBackWallJumping(Vector2 input)
    {
        if (Input.GetButtonDown("Jump"))
        {
            EndWallConnection();
            runner.DoBackWallJump();
        }
    }

    private void HandleSideWallJumping(Vector2 input)
    {
        if (Input.GetButtonDown("Jump"))
        {
            runner.DoSideWallJump(currentWallInfo.wallFacingDirection, climbController.collisions.wallAngle);
            EndWallConnection();
        }
    }

    //Don't put anything in here that has to consistently happen
    private void CheckForClimbTypeChange(Vector2 input)
    {
        int directionX = (potentialWallInfo.climbCollider.transform.position.x > currentWallInfo.climbCollider.transform.position.x) ? 1 : -1;
        int directionY = (potentialWallInfo.climbCollider.transform.position.y > currentWallInfo.climbCollider.transform.position.y) ? 1 : -1;
        Vector2 directionTowardsTransition = new Vector2(directionX, directionY);

        float colliderOverlapLeeway = 0.5f;

        Vector2 modifiedInput;
        switch (potentialWallInfo.inputType)
        {
            case WallInfo.InputType.Horizontal:
            case WallInfo.InputType.Vertical:
                if (Mathf.Abs(input.x) > Mathf.Abs(input.y))
                {
                    modifiedInput = new Vector2(input.x, 0);
                }
                else
                {
                    modifiedInput = new Vector2(0, input.y);
                }
                break;
            default:
                modifiedInput = input;
                break;
        }

        Debug.DrawRay(transform.position + (Vector3)Vector2.up, directionTowardsTransition, Color.white);
        Debug.DrawRay(transform.position, modifiedInput, Color.cyan);

        if (potentialWallInfo.inputType == WallInfo.InputType.Both)
        {
            if (currentWallInfo.inputType == WallInfo.InputType.Both)
            {

            }
            else if (currentWallInfo.inputType == WallInfo.InputType.Horizontal)
            {
                //check if the new climb collider is above or below the current horizontal area
                if ((potentialWallInfo.climbCollider.bounds.max.y < currentWallInfo.climbCollider.bounds.min.y + colliderOverlapLeeway ||
                        potentialWallInfo.climbCollider.bounds.min.y > currentWallInfo.climbCollider.bounds.max.y - colliderOverlapLeeway) &&
                    (potentialWallInfo.climbCollider.bounds.max.x > currentWallInfo.climbCollider.bounds.min.x ||
                        potentialWallInfo.climbCollider.bounds.min.x < currentWallInfo.climbCollider.bounds.max.x))
                {
                    if (modifiedInput.y != 0)
                    {
                        if ((Mathf.Sign(modifiedInput.y) == 1 && directionTowardsTransition.y == 1) ||
                            (Mathf.Sign(modifiedInput.y) == -1 && directionTowardsTransition.y == -1))
                        {
                            TransitionBetweenWallTypes();
                        }
                    }
                }
                //check if the new climb collider is adjacent to the current horizontal area
                else if (potentialWallInfo.climbCollider.bounds.max.x < currentWallInfo.climbCollider.bounds.min.x + colliderOverlapLeeway ||
                            potentialWallInfo.climbCollider.bounds.min.x > currentWallInfo.climbCollider.bounds.max.x - colliderOverlapLeeway)
                {
                    if (modifiedInput.x != 0)
                    {
                        if ((Mathf.Sign(modifiedInput.x) == 1 && directionTowardsTransition.x == 1) ||
                            (Mathf.Sign(modifiedInput.x) == -1 && directionTowardsTransition.x == -1))
                        {
                            TransitionBetweenWallTypes();
                        }
                    }
                }
            }
            else if (currentWallInfo.inputType == WallInfo.InputType.Vertical)
            {
                //check if the climb collider is to the left or the right of the current vertical area
                if ((potentialWallInfo.climbCollider.bounds.max.x < currentWallInfo.climbCollider.bounds.min.x + colliderOverlapLeeway ||
                    potentialWallInfo.climbCollider.bounds.min.x > currentWallInfo.climbCollider.bounds.max.x - colliderOverlapLeeway) &&
                    (potentialWallInfo.climbCollider.bounds.max.y > currentWallInfo.climbCollider.bounds.min.y ||
                    potentialWallInfo.climbCollider.bounds.min.y < currentWallInfo.climbCollider.bounds.max.y))
                {
                    if (input.x != 0)
                    {
                        if ((Mathf.Sign(modifiedInput.x) == 1 && directionTowardsTransition.x == 1) ||
                            (Mathf.Sign(modifiedInput.x) == -1 && directionTowardsTransition.x == -1))
                        {
                            TransitionBetweenWallTypes();
                        }
                    }
                }
                //check if the climb collider is above the current vertical area
                else if (potentialWallInfo.climbCollider.bounds.max.y < currentWallInfo.climbCollider.bounds.min.y + colliderOverlapLeeway ||
                            potentialWallInfo.climbCollider.bounds.min.y > currentWallInfo.climbCollider.bounds.max.y - colliderOverlapLeeway)
                {
                    if (modifiedInput.y != 0)
                    {
                        if ((Mathf.Sign(modifiedInput.y) == 1 && directionTowardsTransition.y == 1) ||
                            (Mathf.Sign(modifiedInput.y) == -1 && directionTowardsTransition.y == -1))
                        {
                            TransitionBetweenWallTypes();
                        }
                    }
                }
            }
        }
        else if (potentialWallInfo.inputType == WallInfo.InputType.Horizontal)
        {
                

            if (modifiedInput.x != 0)
            {
                if (currentWallInfo.inputType == WallInfo.InputType.Both)
                {
                    if (Mathf.Sign(modifiedInput.x) == directionTowardsTransition.x)
                    {
                        TransitionBetweenWallTypes();
                    }
                }
                else
                {
                    //Add special case for wall to ceiling
                    if (currentWallInfo.climbType == ClimbType.SideWall && potentialWallInfo.climbType == ClimbType.Ceiling)
                    {
                        if (isHoldingDirectionSinceClimbTransition)
                        {

                        }
                    }

                    TransitionBetweenWallTypes();
                }
            }
                
        }
        else if (potentialWallInfo.inputType == WallInfo.InputType.Vertical)
        {

            if (modifiedInput.y != 0)
            {
                if (currentWallInfo.inputType == WallInfo.InputType.Both)
                {
                    if (Mathf.Sign(modifiedInput.y) == directionTowardsTransition.y)
                    {
                        TransitionBetweenWallTypes();
                    }
                }
                else
                {
                    //Add special case for ceiling to wall
                    if (currentWallInfo.climbType == ClimbType.Ceiling && potentialWallInfo.climbType == ClimbType.SideWall)
                    {
                        if (isHoldingDirectionSinceClimbTransition)
                        {
                            //Make it so that input changes to purely down
                            input = Vector2.down;
                        }
                    }
                    TransitionBetweenWallTypes();

                    //Make it so if you're on a ceiling pushing away you don't transition
                }
            }
                
            //Add special case for ceiling to wall
            else if (currentWallInfo.climbType == ClimbType.Ceiling && potentialWallInfo.climbType == ClimbType.SideWall)
            {
                TransitionBetweenWallTypes();
                print("Boal");
                if (isHoldingDirectionSinceClimbTransition)
                {
                    //Make it so that input changes to purely down
                    input = Vector2.down;

                }
            }

        }

        
    }

    private void ModifyInput(ref Vector2 input)
    {
        if (isHoldingDirectionSinceClimbTransition)
        {
            print("Balls!");
        }
    }

    private void LogInput(Vector2 input)
    {
        previousInput = input;
    }

    private void CheckIfHoldingDirectionSinceClimbTransition(Vector2 input)
    {
        if (justTransitionedBetweenWallTypes || isHoldingDirectionSinceClimbTransition)
        {
            if (SuperMath.ZeroSign(input.x) == SuperMath.ZeroSign(previousInput.x) &&
                SuperMath.ZeroSign(input.y) == SuperMath.ZeroSign(previousInput.y) &&
                input.y != 0 && input.x != 0)
            {
                isHoldingDirectionSinceClimbTransition = true;
            }
            else
            {
                isHoldingDirectionSinceClimbTransition = false;
            }
        }
    }

    private void TransitionBetweenWallTypes()
    {
        previousWallInfo = currentWallInfo;
        currentWallInfo = potentialWallInfo;
        justTransitionedBetweenWallTypes = true;
    }

    protected override void RunActiveChecks()
    {
        CheckIfCanClimb();
        CheckIfSnapped();
        //DebugWallInfos();
        if(currentWallInfo.climbCollider != null)
        SuperDebugger.DrawPlusAtPoint(currentWallInfo.climbCollider.transform.position, Color.cyan);
    }

    protected override void RunInactiveChecks()
    {
        CheckIfCanClimb();
        CheckForClimbStart();
        //DebugWallInfos();
    }

    private void DebugWallInfos()
    {
        if (currentWallInfo.climbCollider != null)
        {
            print(currentWallInfo.climbCollider.name);
        }
        else
        {
            print("No current");
        }
        if (potentialWallInfo.climbCollider != null)
        {
            print(potentialWallInfo.climbCollider.name);
        }
        else
        {
            print("No potential");
        }
        print("-----------------");
    }

    private Collider2D GetClosestColliderInArea(Vector2 corner, Vector2 oppositeCorner, List<ClimbType> validClimbTypes, bool includeCurrent, Collider2D colliderToIgnore = null)
    {
        Collider2D[] climbColliders = Physics2D.OverlapAreaAll(corner, oppositeCorner, whatIsClimbable);
        Collider2D closest = null;
        float closestLength = float.PositiveInfinity;

        Vector2 center = controller.collider.bounds.center;

        //find closest collider within the climb area
        foreach (Collider2D coll in climbColliders)
        {
            //prevents from checking for a climb collider immediately after leaving that same collider
            if (coll != previousWallInfo.climbCollider)
            {
                if (validClimbTypes.Contains(coll.GetComponent<Climbable>().climbType))
                {
                    if (Vector2.Distance(coll.bounds.ClosestPoint(center), center) < closestLength)
                    {
                        if ((colliderToIgnore != null && coll != colliderToIgnore) || colliderToIgnore == null)
                        {
                            if (!(coll == currentWallInfo.climbCollider && !includeCurrent))
                            {
                                closestLength = Vector2.Distance(coll.bounds.ClosestPoint(center), center);
                                closest = coll;
                            }
                        }
                    }
                }
            }
        }

        return closest;
    }
    

    private void CheckIfCanClimb()
    {
        Collider2D closest = null;
        Collider2D secondClosest = null;
        List<ClimbType> validClimbAreaTypes = new List<ClimbType>()
        {
            ClimbType.BackWall, ClimbType.Ceiling, ClimbType.Vertical, ClimbType.SideWall, ClimbType.Horizontal
        };

        closest = GetClosestColliderInArea(climbCheckArea.bounds.max, climbCheckArea.bounds.min, validClimbAreaTypes, true);
        
        
        if (closest != null)
        {
            canClimb = true;
            secondClosest = GetClosestColliderInArea(climbCheckArea.bounds.max, climbCheckArea.bounds.min, validClimbAreaTypes, true, closest);

            if (isClimbing && secondClosest != null)
            {
                if (currentWallInfo.climbCollider == closest)
                {
                    potentialWallInfo = WallInfo.ConstructFromClimbableCollider(secondClosest);
                }
                else if (currentWallInfo.climbCollider == secondClosest)
                {
                    potentialWallInfo = WallInfo.ConstructFromClimbableCollider(closest);
                }
            }
            else if (isClimbing && secondClosest == null)
            {
                if (currentWallInfo.climbCollider != closest)
                {
                    //if it's the same type, just start climbing on the new one
                    if (currentWallInfo.climbType == closest.GetComponent<Climbable>().climbType)
                    {
                        currentWallInfo = WallInfo.ConstructFromClimbableCollider(closest);
                    }
                    else
                    {
                        print("Premature wall connection end");
                        EndWallConnection();
                    }
                }
                potentialWallInfo.Reset();

            }
            else if (!isClimbing)
            {
                potentialWallInfo = WallInfo.ConstructFromClimbableCollider(closest);
            }
        }
        else
        {
            canClimb = false;
            potentialWallInfo.Reset();
            if (isClimbing)
            {
                EndWallConnection();
            }
        }
    }

    private void EndWallConnection()
    {
        previousWallInfo = currentWallInfo;
        currentWallInfo.Reset();
        isClimbing = false;
        Invoke("ForgetPreviousWallInfo", previousWallMemoryLength);
    }

    private void ForgetPreviousWallInfo()
    {
        previousWallInfo.Reset();
    }

    private void CheckIfSnapped()
    {
        isSnappedToClimbable = false; 

        switch (currentWallInfo.climbType)
        {
            case ClimbType.None:
                break;
            case ClimbType.BackWall:
                isSnappedToClimbable = true;
                break;
            case ClimbType.Horizontal:
                if (Mathf.Abs(currentWallInfo.climbCollider.bounds.center.y - ledgeCheckArea.bounds.center.y) <= snapDistanceLeeway)
                {
                    isSnappedToClimbable = true;
                }
                break;
            case ClimbType.Vertical:
                if (Mathf.Abs(currentWallInfo.climbCollider.bounds.center.x - transform.position.x) <= snapDistanceLeeway)
                {
                    isSnappedToClimbable = true;
                }
                break;
            case ClimbType.SideWall:
                break;
            case ClimbType.Ceiling:
                break;
        }
    }

    private void CheckForClimbStart()
    {
        //Press up to start climbing
        //if (canClimb && Input.GetAxis("Vertical") > climbVerticalInputThreshold)

        //Press 'grab' to start climbing
        if (canClimb && Input.GetButton("Grab"))
        {
            isClimbing = true;
            currentWallInfo = potentialWallInfo;
            potentialWallInfo.Reset();
        }
    }

    private void SnapIfNecessary()
    {
        if (isClimbing && !isSnappedToClimbable)
        {
            switch (currentWallInfo.climbType)
            {
                case ClimbType.BackWall:
                    break;
                case ClimbType.Horizontal:
                    SnapToLedge();
                    break;
                case ClimbType.Vertical:
                    SnapToVertical();
                    break;
                case ClimbType.SideWall:
                    SnapToWall();
                    break;
                case ClimbType.Ceiling:
                    SnapToCeiling();
                    break;
            }
        }
    }

    private void SnapToLedge()
    {
        float verticalDistanceToLedge = Mathf.Abs(currentWallInfo.climbCollider.bounds.center.y - ledgeCheckArea.bounds.center.y);
        int verticalDirection = (currentWallInfo.climbCollider.bounds.center.y < ledgeCheckArea.bounds.center.y) ? -1 : 1;

        velocity = new Vector3(0, verticalDistanceToLedge * verticalDirection, 0);
    }

    private void SnapToVertical()
    {
        float horizontalDistanceToClimbArea = Mathf.Abs(currentWallInfo.climbCollider.bounds.center.x - transform.position.x);
        int horizontalDirection = (currentWallInfo.climbCollider.bounds.center.x < transform.position.x) ? -1 : 1;

        velocity = new Vector3(horizontalDistanceToClimbArea * horizontalDirection, 0, 0);
    }

    private void SnapToWall()
    {

    }

    private void SnapToCeiling()
    {

    }


    public struct WallInfo
    {
        public enum InputType
        {
            None,
            Horizontal,
            Vertical,
            Both
        };

        public ClimbType climbType;
        public WallFacingDirection wallFacingDirection;
        public Collider2D climbCollider;
        public InputType inputType;

        public void Reset()
        {
            climbType = ClimbType.None;
            wallFacingDirection = WallFacingDirection.None;
            climbCollider = null;
            inputType = InputType.None;
        }

        public static WallInfo ConstructFromClimbableCollider(Collider2D collider)
        {
            Climbable climbableComponent;
            WallInfo newWallInfo = new WallInfo();
            if (climbableComponent = collider.GetComponent<Climbable>())
            {
                newWallInfo.climbCollider = collider;
                newWallInfo.climbType = climbableComponent.climbType;
                newWallInfo.wallFacingDirection = climbableComponent.facingDirection;
                switch (newWallInfo.climbType)
                {
                    case ClimbType.SideWall:
                    case ClimbType.Vertical:
                        newWallInfo.inputType = InputType.Vertical;
                        break;
                    case ClimbType.Horizontal:
                    case ClimbType.Ceiling:
                        newWallInfo.inputType = InputType.Horizontal;
                        break;
                    case ClimbType.BackWall:
                        newWallInfo.inputType = InputType.Both;
                        break;
                    default:
                        newWallInfo.inputType = InputType.None;
                        break;
                }
            }
            else
            {
                newWallInfo.Reset();
            }

            return newWallInfo;
        }
    }

}
