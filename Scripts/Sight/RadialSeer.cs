﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class RadialSeer : MonoBehaviour
{
    private List<Seeable> allSeeablesSeen = new List<Seeable>();

    public LayerMask seeableLayer;
    public LayerMask obstacleLayer;
    private LayerMask combinedSeeableLayer;

    public int rayCount = 9;

    private Vector2 sightDirection = Vector2.right;

    public float sightStartDistanceFromCenter = 0.15f;
    public float sightLength = 2.5f;
    public float sightWidthAtStart = 0.1f;
    public float sightWidthAtEnd = 1.4f;

    public bool debugGeneral = false;
    public bool debugSeeableSight = false;
    public bool debugHoppableSight = false;

    private bool canTurn = true;
    public bool CanTurn
    {
        get
        {
            return canTurn;
        }
    }
    public void AllowTurning()
    {
        canTurn = true;
    }
    public void DisallowTurning()
    {
        canTurn = false;
    }

    public Seeable GetNearestSeeable
    {
        get
        {
            if (allSeeablesSeen.Count > 0)
            {
                if (allSeeablesSeen.Count == 1)
                {
                    return allSeeablesSeen[0];
                }
                else
                {
                    return GetSeeablesSortedNearestFirst[0];
                }
            }
            else
            {
                return null;
            }
        }
    }

    public List<Seeable> GetSeeablesSortedNearestFirst
    {
        get
        {
            if (allSeeablesSeen.Count > 0)
            {
                if (allSeeablesSeen.Count == 1)
                {
                    return allSeeablesSeen;
                }
                else
                {
                    return allSeeablesSeen.OrderBy(e => Vector2.Distance(transform.position, e.transform.position)).ToList();
                }
            }
            else
            {
                return null;
            }
        }
    }

    private void Start()
    {
        combinedSeeableLayer = obstacleLayer | seeableLayer;
    }

    private void CalculateSightDirection(PlayerActions playerActions)
    {
        sightDirection = (playerActions.Move.X == 0 && playerActions.Move.Y == 0) ? sightDirection : playerActions.Move;
        sightDirection.Normalize();

        if (debugGeneral)
        {
            Debug.DrawRay(transform.position, sightDirection, Color.cyan);
        }
    }

    public void See(PlayerActions playerActions)
    {
        if (CanTurn)
        {
            CalculateSightDirection(playerActions);
        }

        allSeeablesSeen.Clear();

        //Find start and end points based off of Sight Direction
        Vector2 orthogonalSightVector = new Vector2(-sightDirection.y, sightDirection.x);

        Vector2[] startPoints = new Vector2[2];
        Vector2 centralSightStartPosition = (sightDirection * sightStartDistanceFromCenter) + (Vector2)transform.position;
        startPoints[0] = centralSightStartPosition + (orthogonalSightVector * (sightWidthAtStart / 2));
        startPoints[1] = centralSightStartPosition - (orthogonalSightVector * (sightWidthAtStart / 2));

        Vector2[] endPoints = new Vector2[2];
        Vector2 centralSightEndPosition = (sightDirection * sightLength) + (Vector2)transform.position;
        endPoints[0] = centralSightEndPosition + (orthogonalSightVector * (sightWidthAtEnd / 2));
        endPoints[1] = centralSightEndPosition - (orthogonalSightVector * (sightWidthAtEnd / 2));

        if (debugGeneral)
        {
            SuperDebugger.DrawPlusAtPoint(startPoints[0], Color.green, 0.1f);
            SuperDebugger.DrawPlusAtPoint(startPoints[1], Color.green, 0.1f);
            SuperDebugger.DrawPlusAtPoint(endPoints[0], Color.red, 0.1f);
            SuperDebugger.DrawPlusAtPoint(endPoints[1], Color.red, 0.1f);
        }

        //Getting ready for raycasting
        Vector2 startOrigin = startPoints[0];
        Vector2 endOrigin = endPoints[0];
        Vector2 betweenStartPoints = startPoints[1] - startPoints[0];
        Vector2 betweenEndPoints = endPoints[1] - endPoints[0];
        float distanceBetweenStartPoints = betweenStartPoints.magnitude;
        float distanceBetweenEndPoints = betweenEndPoints.magnitude;
        float startPointOffset = distanceBetweenStartPoints / (rayCount - 1);
        float endPointOffset = distanceBetweenEndPoints / (rayCount - 1);

        //Cast Rays
        for (int i = 0; i < rayCount; i++)
        {
            Vector2 origin = startOrigin - (orthogonalSightVector * startPointOffset * i);
            Vector2 end = endOrigin - (orthogonalSightVector * endPointOffset * i);
            Vector2 toEnd = end - origin;

            float distanceToEnd = toEnd.magnitude;
            Vector2 directionToEnd = toEnd.normalized;

            
            //Check for Seeable objects
            RaycastHit2D seeableHit = Physics2D.Raycast(origin, directionToEnd, distanceToEnd, combinedSeeableLayer);
            if (seeableHit)
            {
                distanceToEnd = seeableHit.distance;

                //Hit a seeable object (not just an obstacle)
                if (1 << seeableHit.collider.gameObject.layer == seeableLayer.value)
                {
                    Seeable foundSeeable = seeableHit.collider.gameObject.GetComponent<Seeable>();
                    if (foundSeeable != null && !allSeeablesSeen.Contains(foundSeeable))
                    {
                        foundSeeable.WasSeen(this);
                        allSeeablesSeen.Add(foundSeeable);
                    }
                    else if (foundSeeable == null)
                    {
                        Debug.LogWarning("WARNING: Found an object on layer Seeable without a Seeable component");
                    }
                }
            }

            if (debugSeeableSight)
            {
                Debug.DrawRay(origin, directionToEnd * distanceToEnd, Color.magenta);

                foreach (Seeable seeable in allSeeablesSeen)
                {
                    SuperDebugger.DrawPlusAtPoint(seeable.transform.position, Color.magenta, 0.3f);
                }
            }
        }
    }








}
