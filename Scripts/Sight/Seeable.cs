﻿using UnityEngine;
using System.Collections;

public class Seeable : MonoBehaviour 
{
    private bool isBeingSeen = false;
    public bool IsBeingSeen
    {
        get
        {
            return isBeingSeen;
        }
    }

    private bool wasSeenLastFrame = false;
    private RadialSeer currentSeer;

    //Events
    public delegate void SeenAction(RadialSeer seer);
    public event SeenAction OnStartedBeingSeen;
    public delegate void UnseenAction();
    public event UnseenAction OnNoLongerSeen;

    private void Start()
    {
        if (gameObject.layer != LayerMask.NameToLayer("Seeable"))
        {
            gameObject.layer = LayerMask.NameToLayer("Seeable");
        }
    }

    private void Update()
    {
        if (wasSeenLastFrame && !isBeingSeen)
        {
            if (OnNoLongerSeen != null)
            {
                OnNoLongerSeen();
            }
        }
        else if (isBeingSeen && !wasSeenLastFrame)
        {
            if (OnStartedBeingSeen != null)
            {
                OnStartedBeingSeen(currentSeer);
            }
        }

        wasSeenLastFrame = isBeingSeen;
        isBeingSeen = false;
        currentSeer = null;
    }

    public void WasSeen(RadialSeer seer)
    {
        //print("I, " + gameObject.name + ", was seen by " + seer.gameObject.name + "!");
        isBeingSeen = true;
        currentSeer = seer;
    }

}
