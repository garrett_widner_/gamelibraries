﻿using UnityEngine;
using InControl;

public class PlayerController : MonoBehaviour 
{
    private float overallLowerDeadZone = 0.4f;

    private PlayerActions playerActions;
    public GroundMover groundMover;
    public PlayerHopHandler playerHopHandler;
    public HopMover hopMover;
    public PlayerPickupCollector pickupCollector;
    public PlayerPilePlacer pilePlacer;
    public PlayerDialogueTrigger dialogueTrigger;
    public PauseManager pauseManager;

    private void Start()
    {
        Restart();
    }

    public void Restart()
    {
        playerActions = PlayerActions.CreateWithDefaultBindings();
        playerHopHandler.RunSetup(playerActions);
        groundMover.RunSetup(playerActions);
        
        //print("The player controller (re)started");

    }

    public void OnEnable()
    {
        playerHopHandler.OnHopChainStarted += ConstrainMovement;
        playerHopHandler.OnHopChainEnded += UnconstrainMovement;
    }

    public void OnDisable()
    {
        playerHopHandler.OnHopChainStarted -= ConstrainMovement;
        playerHopHandler.OnHopChainEnded -= UnconstrainMovement;
    }

    private void Update()
    {
        SetInputDeviceDeadZone();

        Vector2 velocity = Vector2.zero;

        if (!hopMover.IsHopping)
        {
            groundMover.Move(ref velocity);

            
        }
        else if(hopMover.IsHopping)
        {
            hopMover.Move();
        }

        if (playerActions.Pickup.WasPressed)
        {
            pickupCollector.CheckForPickup();
            dialogueTrigger.CheckForDialogue();
            pilePlacer.CheckForPilePlacement();
        }

        if (!hopMover.IsHopping)
        {
            transform.Translate(velocity);
        }
    }

    private void SetInputDeviceDeadZone()
    {
        InputDevice inputDevice = InputManager.ActiveDevice;
        if (inputDevice.DeviceClass == InputDeviceClass.Controller)
        {
            inputDevice.LeftStickLeft.LowerDeadZone = overallLowerDeadZone;
            inputDevice.LeftStickRight.LowerDeadZone = overallLowerDeadZone;
            inputDevice.LeftStickUp.LowerDeadZone = overallLowerDeadZone;
            inputDevice.LeftStickDown.LowerDeadZone = overallLowerDeadZone;
        }
    }

    private void ConstrainMovement()
    {
        groundMover.DisallowMovement();
    }

    private void UnconstrainMovement()
    {
        if (!pauseManager.IsPaused)
        {
            groundMover.AllowMovement();
        }
    }

}
