﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxDirectionChecker : MonoBehaviour
{
    public static Vector2 CheckRelativeDirection(BoxCollider2D mainBox, BoxCollider2D secondaryBox)
    {
        //Find relative ordinal direction
        Vector2 relativeOrdinal = new Vector2(0, 0);

        if(secondaryBox.transform.position.x < mainBox.transform.position.x)
        {
            relativeOrdinal.x = -1;
        }
        else
        {
            relativeOrdinal.x = 1;
        }

        if (secondaryBox.transform.position.y < mainBox.transform.position.y)
        {
            relativeOrdinal.y = -1;
        }
        else
        {
            relativeOrdinal.y = 1;
        }

        /*
        //Figure out whether you're MOSTLY above/below or left/right
        bool isLeftRight = false;
        if(relativeOrdinal.y > 0 && relativeOrdinal.x > 0)
        {
            if(Mathf.Abs(mainBox.bounds.max.y - secondaryBox.bounds.min.y) > Mathf.Abs(mainBox.bounds.max.x - secondaryBox.bounds.min.x))
            {
                isLeftRight = true;
            }
            else
            {
                isLeftRight = false;
            }

        }
        else if(relativeOrdinal.y < 0 && relativeOrdinal.x > 0)
        {
            if (Mathf.Abs(mainBox.bounds.min.y - secondaryBox.bounds.max.y) > Mathf.Abs(mainBox.bounds.max.x - secondaryBox.bounds.min.x))
            {
                isLeftRight = true;
            }
            else
            {
                isLeftRight = false;
            }
        }
        else if(relativeOrdinal.y > 0 && relativeOrdinal.x < 0)
        {
            if(Mathf.Abs(mainBox.bounds.max.y - secondaryBox.bounds.min.y) > Mathf.Abs(mainBox.bounds.min.x - secondaryBox.bounds.max.x))
            {
                isLeftRight = true;
            }
            else
            {
                isLeftRight = false;
            }
        }
        else if(relativeOrdinal.y < 0 && relativeOrdinal.x < 0)
        {
            if(Mathf.Abs(mainBox.bounds.min.y - secondaryBox.bounds.max.y) > Mathf.Abs(mainBox.bounds.min.x - secondaryBox.bounds.max.x))
            {
                isLeftRight = true;
            }
            else
            {
                isLeftRight = false;
            }
        }
        */

        //Determine whether ordinal or cardinal direction should be used
        //if mostly above/below use IsInXBounds, otherwise IsInYBounds
        
        if (IsInXBounds(mainBox, secondaryBox, true, relativeOrdinal))
        {
            relativeOrdinal.x = 0;
        }
        else if(IsInYBounds(mainBox, secondaryBox, true, relativeOrdinal))
        {
            relativeOrdinal.y = 0;
        }

        print(relativeOrdinal);
        Debug.DrawRay(mainBox.transform.position, relativeOrdinal);

        return relativeOrdinal;
    }

    public static bool IsInXBounds(BoxCollider2D mainBox, BoxCollider2D secondaryBox, bool halfwayCounts, Vector2 relativeOrdinal)
    {
        bool isInBounds = false;


        if((mainBox.bounds.min.x < secondaryBox.bounds.min.x) && (mainBox.bounds.max.x > secondaryBox.bounds.max.x))
        {
            isInBounds = true;
        }
        else if((secondaryBox.bounds.min.x < mainBox.bounds.min.x) && (secondaryBox.bounds.max.x > mainBox.bounds.max.x))
        {
            isInBounds = true;
        }

        if (halfwayCounts && !isInBounds)
        {
            if(relativeOrdinal.x == -1)
            {
                if (mainBox.bounds.center.x < secondaryBox.bounds.max.x)
                {
                    isInBounds = true;
                }
            }
            else
            {
                if(mainBox.bounds.center.x > secondaryBox.bounds.min.x)
                {
                    isInBounds = true;
                }
            }
        }

        return isInBounds;
    }

    public static bool IsInYBounds(BoxCollider2D mainBox, BoxCollider2D secondaryBox, bool halfwayCounts, Vector2 relativeOrdinal)
    {
        bool isInBounds = false;


        if ((mainBox.bounds.min.y < secondaryBox.bounds.min.y) && (mainBox.bounds.max.y > secondaryBox.bounds.max.y))
        {
            isInBounds = true;
        }
        else if ((secondaryBox.bounds.min.y < mainBox.bounds.min.y) && (secondaryBox.bounds.max.y > mainBox.bounds.max.y))
        {
            isInBounds = true;
        }

        if (halfwayCounts && !isInBounds)
        {
            if (relativeOrdinal.y == -1)
            {
                if (mainBox.bounds.center.y < secondaryBox.bounds.max.y)
                {
                    isInBounds = true;
                }
            }
            else
            {
                if (mainBox.bounds.center.y > secondaryBox.bounds.min.y)
                {
                    isInBounds = true;
                }
            }
        }

        return isInBounds;
    }


}
